![picture](https://shippmedia.s3.sa-east-1.amazonaws.com/popups/desafioshipp.png)

# Desafio Shipp Estagio

### Barra de pesquisa de usuário do GitHub.

O teste deve ser feito para web deve ser utilizado apenas HTML, CSS e JS (podendo usar qualquer framework javascript, como jQuery ou angular). 

Deverá ser desenvolvido um buscador de usuários do gitHub utilizando os end-points do próprio gitHub.

O usuário deverá digitar o login em uma barra des pesquisa e exibir uma listagem com o resultado da pesquisa em forma de card, que devem conter: 

+ Imagem do usuário
+ Nick do usuário
+ Link do GitHub do usuário
+ Score do usuário

O usuário deverá poder ver os detalhes do card selecionado, atraves de um modal que deve conter: 

+ Imagem do usuário
+ Nome do usuário
+ Nick do usuário 
+ Link do GitHub do usuário
+ Email do usuário
+ Quantas pessoas o usuário segue
+ Quantos pessoas estão seguindo o usuário 
+ Url do blog do usuário 
+ Data do cadatro do usuário

Devem ser usadas boas práticas de programação, como comentarios e modularização de funções. Devem ser seguidas as distancias das especificações de layout.

Documentação GitHub para pesquisa de usuários: https://developer.github.com/v3/search
Documentação GitHub para obter um usuário: https://developer.github.com/v3/users

Link do prototipo para entendimento da interface: https://xd.adobe.com/view/d9151c28-5bf0-429f-77b7-35303741c2a3-af24/?fullscreen 
O layout está disponível em: https://xd.adobe.com/spec/d2af8939-d5dd-4e8a-4628-e6e83de3ed88-a49e/

-----
###### Tela de busca

Para obter uma lista de usuarios do GitHub atraves de uma query string atraves de uma chamda Ajax.

Documentação https://developer.github.com/v3/search/#search-users

(GET) https://api.github.com/search/users?q=:username

REQUEST 

``` 
	url https://api.github.com/search/users?q=mojomb
	json {}
```

RESPONSE

``` 
	json
	{
	  "total_count": 12,
	  "incomplete_results": false,
	  "items": [
		{
		  "login": "mojombo",
		  "id": 1,
		  "node_id": "MDQ6VXNlcjE=",
		  "avatar_url": "https://secure.gravatar.com/avatar/25c7c18223fb42a4c6ae1c8db6f50f9b?d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png",
		  "gravatar_id": "",
		  "url": "https://api.github.com/users/mojombo",
		  "html_url": "https://github.com/mojombo",
		  "followers_url": "https://api.github.com/users/mojombo/followers",
		  "subscriptions_url": "https://api.github.com/users/mojombo/subscriptions",
		  "organizations_url": "https://api.github.com/users/mojombo/orgs",
		  "repos_url": "https://api.github.com/users/mojombo/repos",
		  "received_events_url": "https://api.github.com/users/mojombo/received_events",
		  "type": "User",
		  "score": 105.47857
		}
	  ]
	}
```
Devem ser utilizado os campos: (descrição -> valor)

+ Imagem do usuário -> avatar_url
+ Nick do usuário -> login
+ Link do GitHub do usuário -> html_url
+ Score do usuário -> score

-----
###### Modal de Usuario do GitHub

Para recuperar os dados do usuario do gitHub: Deve ser implementado uma chamada ajax utilizando o campo "url" da chamada feita acima para obter os dados do usuario.
Documentação https://developer.github.com/v3/users/

(GET) https://api.github.com/users/:username

REQUEST

```
	url https://api.github.com/users/mojombo
	json {}
```

RESPONSE

``` 
	json
	{
	  "login": "octocat",
	  "id": 1,
	  "node_id": "MDQ6VXNlcjE=",
	  "avatar_url": "https://github.com/images/error/octocat_happy.gif",
	  "gravatar_id": "",
	  "url": "https://api.github.com/users/octocat",
	  "html_url": "https://github.com/octocat",
	  "followers_url": "https://api.github.com/users/octocat/followers",
	  "following_url": "https://api.github.com/users/octocat/following{/other_user}",
	  "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
	  "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
	  "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
	  "organizations_url": "https://api.github.com/users/octocat/orgs",
	  "repos_url": "https://api.github.com/users/octocat/repos",
	  "events_url": "https://api.github.com/users/octocat/events{/privacy}",
	  "received_events_url": "https://api.github.com/users/octocat/received_events",
	  "type": "User",
	  "site_admin": false,
	  "name": "monalisa octocat",
	  "company": "GitHub",
	  "blog": "https://github.com/blog",
	  "location": "San Francisco",
	  "email": "octocat@github.com",
	  "hireable": false,
	  "bio": "There once was...",
	  "public_repos": 2,
	  "public_gists": 1,
	  "followers": 20,
	  "following": 0,
	  "created_at": "2008-01-14T04:33:35Z",
	  "updated_at": "2008-01-14T04:33:35Z"
	}
```
Devem ser Utilizado os campos: (descrição -> valor)

+ Imagem do usuário -> avatar_url
+ Nome do usuário -> name
+ Nick do usuário -> login
+ Link do GitHub do usuário -> html_url
+ Email do usuário -> email
+ Quantas pessoas o usuário segue -> following
+ Quantos pessoas estão seguindo o usuário -> followers
+ Url do blog do usuário -> blog
+ Data do cadatro do usuário -> created_at
